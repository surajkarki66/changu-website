from django.db import models
from datetime import datetime


class Notice(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='image/')
    pub_date = models.DateTimeField(default=datetime.now(), blank=True)
    body = models.TextField()
    file = models.FileField(upload_to='files/')

    def date(self):
        self.date_b = self.pub_date.strftime('%d/%e/%Y')
        return self.date_b

    def __str__(self):
        return self.title + \
          " Upload on: " + \
        str(self.pub_date.strftime("%Y/%m/%d"))


class ContactInfo(models.Model):
    name = models.CharField(max_length=200)
    pub_date = models.DateTimeField(default=datetime.now(), blank=True)
    message = models.TextField()
    email = models.CharField(max_length=300)

    def date(self):
        self.date_b = self.pub_date.strftime('%d/%e/%Y')
        return self.date_b

    def __str__(self):
        return self.email + \
          " Upload on: " + \
        str(self.pub_date.strftime("%Y/%m/%d"))
