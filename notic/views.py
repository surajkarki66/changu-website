from django.shortcuts import render, redirect
from news.models import News
from .models import Notice, ContactInfo
from django.utils import timezone


def home(request):
    news = News.objects
    notice = Notice.objects
    content = {
        'news_home': news,
        'notice': notice,
    }
    return render(request, 'home.html', content)


def notice_home(request):
    notice = Notice.objects
    content = {
        'notice': notice,
    }
    return render(request, 'notice/notice.html', content)


def contact(request):
    if request.method == 'POST':
        contact = ContactInfo()
        contact.name = request.POST['name']
        contact.message = request.POST['message']
        contact.email = request.POST['email']
        contact.pub_date = timezone.datetime.now()
        contact.save()
        return redirect('contact')
    else:
        return render(request, 'contact.html')
