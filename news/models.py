from django.db import models
from datetime import datetime
from django.urls import reverse

class News(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='image/')
    pub_date = models.DateTimeField(default=datetime.now(), blank=True)
    body = models.TextField()

    def __str__(self):
        return self.title

    def date(self):
        return self.pub_date.strftime('%d/%e/%Y')

    def get_body(self):
        return self.body[:100]

    def get_absolute_url(self):
        return reverse('detail', kwargs={
            'id':self.id
        })
