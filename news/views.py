from django.shortcuts import render, get_object_or_404 ,redirect, reverse
from .models import News
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def news_home(request):


    news_list = News.objects.all()



    news = News.objects.order_by('-pub_date')[:5]

    paginator = Paginator(news_list, 2)
    page_request_var = 'page'
    page = request.GET.get(page_request_var)
    try:
        paginated_queryset = paginator.page(page)
    except PageNotAnInteger:
        paginated_queryset = paginator.page(1)
    except EmptyPage:
        paginated_queryset = paginator.page(paginator.num_pages)
    
	
    content = {
        'queryset': paginated_queryset,
        'news_home': news,
        'page_request_var': page_request_var
    }
    return render(request, 'news/news.html', content)


def news_detail(request, id):
    news = get_object_or_404(News, id=id)
    if request.method == 'POST':
        return redirect(reverse('detail', kwargs={
            'id': news_detail.pk,
        }))
    content = {
        'news': news
    }
    return render(request, 'news/news_detail.html', content)
